use rand::distributions::{uniform::SampleUniform, Distribution, Uniform};
use rand::thread_rng;
use serde::{Deserialize, Serialize};
use serde_json::{from_str, to_string};
use std::collections::HashMap;
use structopt::StructOpt;

fn rand_in_range<T>(min: T, max: T) -> T
where
    T: SampleUniform,
{
    let range = Uniform::new(min, max);
    range.sample(&mut thread_rng())
}

type Id = usize;

#[derive(Debug, Serialize, Deserialize)]
struct TestData {
    first_names: Vec<String>,
    last_names: Vec<String>,
    age_range: [usize; 2],
    locations: HashMap<String, Vec<String>>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Person {
    id: Id,
    first_name: String,
    last_name: String,
    age: usize,
    city: String,
    state: String,
    connections: Vec<(Id, i64)>,
}

#[derive(Debug, StructOpt)]
#[structopt(name = "gen-people", about = "generate data for love-triangles")]
struct Args {
    #[structopt(short, long)]
    output: String,

    #[structopt(short, long)]
    people: usize,

    #[structopt(short, long)]
    triangles: usize,
}

fn main() {
    let args: Args = Args::from_args();
    let test_data_json = String::from_utf8_lossy(include_bytes!("test_data.json"));
    let test_data = from_str::<TestData>(&test_data_json).expect("failed to parse test_data.json");

    let states = test_data
        .locations
        .iter()
        .map(|(k, _)| k.clone())
        .collect::<Vec<String>>();

    let location = || -> (String, String) {
        let state = states[rand_in_range(0, states.len())].to_string();
        let cities = &test_data.locations[&state];
        let city = cities[rand_in_range(0, cities.len())].clone();
        (state, city)
    };

    let first_name =
        || test_data.first_names[rand_in_range(0, test_data.first_names.len())].clone();
    let last_name = || test_data.last_names[rand_in_range(0, test_data.last_names.len())].clone();
    let new_age = || rand_in_range(test_data.age_range[0], test_data.age_range[1]);

    let person = |data: (usize, [(usize, i64); 2])| {
        let (state, city) = location();
        Person {
            id: data.0,
            first_name: first_name(),
            last_name: last_name(),
            state,
            city,
            age: new_age(),
            connections: data.1.iter().map(|e| e.clone()).collect(),
        }
    };

    let people = (0..args.triangles)
        .map(|idx| {
            let offset = idx * 3;
            let (a, b, c) = (offset, offset + 1, offset + 2);
            let pos: (i64, i64, i64) = (
                rand_in_range(1, 10),
                rand_in_range(1, 10),
                rand_in_range(1, 10),
            );
            let neg: (i64, i64, i64) = (
                rand_in_range(-10, -1),
                rand_in_range(-10, -1),
                rand_in_range(-10, -1),
            );
            let edges: [(usize, [(usize, i64); 2]); 3] = [
                (a, [(b, pos.0), (c, neg.0)]),
                (b, [(c, pos.1), (a, neg.1)]),
                (c, [(a, pos.2), (b, neg.2)]),
            ];
            [person(edges[0]), person(edges[1]), person(edges[2])]
        })
        .fold(vec![], |mut v, people| {
            for p in people.iter() {
                v.push(p.clone());
            }
            v
        });

    let new_people = (people.len()..args.people)
        .map(|id| {
            let connections: Vec<(usize, i64)> = (0..rand_in_range(0, args.people - id))
                .map(|_| (rand_in_range(id + 1, args.people), rand_in_range(-10, 10)))
                .collect::<HashMap<usize, i64>>()
                .iter()
                .map(|(k, v)| (*k, *v))
                .collect();

            let (state, city) = location();
            Person {
                id,
                state,
                city,
                first_name: first_name(),
                last_name: last_name(),
                age: new_age(),
                connections,
            }
        })
        .collect::<Vec<Person>>();

    std::fs::write(&args.output, to_string(&people).unwrap()).expect("failed to write output file");
}
