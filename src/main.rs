use serde::{Deserialize, Serialize};
use serde_json as json;
use std::fs as file;
use structopt::StructOpt;

mod graph;
use graph::{is_love_triangle, Graph};
pub type Idx = usize;
pub type Weight = i64;
pub type Edge = (Idx, Weight);
pub type Label = u64;

const POS: Label = 1 << 0;
const NEG: Label = 1 << 1;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Input {
    id: Idx,
    first_name: String,
    last_name: String,
    age: Weight,
    city: String,
    state: String,
    connections: Vec<(Idx, Weight)>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Output {
    people: (Input, Input, Input),
    passion: f64,
}

#[derive(Debug, StructOpt)]
#[structopt(
    name = "love-triangles",
    about = "Find the love triangles in a population"
)]
struct Args {
    #[structopt(short, long)]
    input: String,
    #[structopt(short, long)]
    output: String,
}

fn main() {
    // 1. read and validate input
    let args: Args = Args::from_args();
    let output_path: &str = &args.output;
    let input_json = file::read_to_string(&args.input).expect("failed to read input file");
    let input: Vec<Input> = json::from_str(&input_json).expect("failed to parse input json");

    let mut graph = input
        .iter()
        .map(|person| (person.id, person.connections.as_slice()))
        .collect::<Graph>();

    graph.label_vertices(|g, v| {
        let mut label = 0;
        for (_, w) in g.get_edges(v) {
            if *w > 0 {
                label = label | POS;
            } else if *w < 0 {
                label = label | NEG;
            }
        }
        label
    });

    let output: Vec<Output> = graph
        .iter()
        .filter(|it| graph.vertex_labels[it.0] == POS | NEG)
        .collect::<Graph>()
        .find_triangles(is_love_triangle)
        .iter()
        .map(|(a, b, c)| {
            let norm = |elem: (f64, f64, f64)| -> f64 {
                (elem.0.powf(2.0) + elem.1.powf(2.0) + elem.2.powf(2.0)).sqrt()
            };

            let factor = |a: Idx, b: Idx, c: Idx| {
                let delta_age = (
                    (input[a].age - input[b].age) as f64,
                    (input[b].age - input[c].age) as f64,
                    (input[c].age - input[a].age) as f64,
                );

                let weight = (
                    graph.weight(a, b).unwrap() as f64,
                    graph.weight(b, c).unwrap() as f64,
                    graph.weight(c, a).unwrap() as f64,
                );

                norm(weight) / (norm(delta_age) + 1.0)
            };

            let love = factor(*a, *b, *c);
            let hate = factor(*c, *b, *a);

            Output {
                passion: love * hate,
                people: (input[*a].clone(), input[*b].clone(), input[*c].clone()),
            }
        })
        .collect();

    println!("Found {} love triangles", output.len());
    file::write(output_path, &json::to_string_pretty(&output).unwrap())
        .expect("failed to write to output file");
}
