use super::{Edge, Idx, Label, Weight};
use std::collections::HashSet;
use std::iter::FromIterator;

/// The graph representation, as a struct of arrays.
pub struct Graph {
    vertex_count: usize,
    adj_span: Vec<(usize, usize)>,
    adj_list: Vec<Edge>,
    pub vertex_labels: Vec<Label>,
}

/// An iterator over the vertices of the graph
pub struct GraphIter<'a> {
    idx: usize,
    g: &'a Graph,
}

/// Intermediate structure, used to prevent repetitions
#[derive(Hash, PartialEq, Eq)]
pub struct Trio {
    v_a: Idx,
    v_b: Idx,
    v_c: Idx,
}

// This is idiomatic-ish Rust code, it allows iterator combinators
// to be collected into a graph. I used this to deal with data conversion.
impl<'a> FromIterator<(Idx, &'a [Edge])> for Graph {
    fn from_iter<It>(it: It) -> Self
    where
        It: IntoIterator<Item = (Idx, &'a [Edge])>,
    {
        let elements: Vec<It::Item> = it.into_iter().collect();
        let count = elements.len();
        let mut self_ = Self {
            vertex_count: count,
            adj_span: Vec::with_capacity(count),
            adj_list: Vec::with_capacity(count * count),
            vertex_labels: Vec::with_capacity(count),
        };

        let mut offset = 0;
        for (_, e) in elements.iter() {
            self_.adj_span.push((offset, e.len()));
            for edge in *e {
                self_.adj_list.push(*edge);
            }
            offset = offset + e.len();
        }
        self_.vertex_labels.resize(count, 0);
        self_
    }
}

impl Graph {
    /// returns the weight of an edge
    pub fn weight(&self, first: Idx, second: Idx) -> Option<Weight> {
        for (v, w) in self.get_edges(first) {
            if *v == second {
                return Some(*w);
            }
        }
        None
    }
    /// returns an iterator over the graph
    pub fn iter(&self) -> GraphIter {
        GraphIter { idx: 0, g: &self }
    }

    /// Returns the edges for a given vertex
    pub fn get_edges(&self, v: Idx) -> &[Edge] {
        // handles the case where v isn't actually in the graph. Probably more
        // sensible to return an option/result type, but this doesn't break things.
        if v >= self.vertex_count {
            &[]
        } else {
            let span = self.adj_span[v];
            self.adj_list
                .as_slice()
                .split_at(span.0)
                .1
                .split_at(span.1)
                .0
        }
    }

    /// returns all the triangles in the graph.
    pub fn find_triangles<F>(&self, predicate: F) -> Vec<(Idx, Idx, Idx)>
    where
        F: Fn(&Graph, Idx, Idx, Idx) -> bool,
    {
        let mut set = HashSet::new();

        for v_a in 0..self.vertex_count {
            'l0: for (v_b, _) in self.get_edges(v_a) {
                if *v_b == v_a {
                    continue 'l0;
                }

                'l1: for (v_c, _) in self.get_edges(*v_b) {
                    if (*v_c == *v_b) || (*v_c == v_a) {
                        continue 'l1;
                    }

                    for (v_d, _) in self.get_edges(*v_c) {
                        if (*v_d == v_a) && predicate(&self, v_a, *v_b, *v_c) {
                            let mut result = [v_a, *v_b, *v_c];
                            result.sort();
                            set.insert(Trio {
                                v_a: result[0],
                                v_b: result[1],
                                v_c: result[2],
                            });
                        }
                    }
                }
            }
        }

        set.iter()
            .map(|trio| (trio.v_a, trio.v_b, trio.v_c))
            .collect()
    }

    /// Applies a label to a given vertex
    pub fn label_vertices<F>(&mut self, func: F)
    where
        F: Fn(&Graph, Idx) -> Label,
    {
        for v in 0..self.vertex_count {
            self.vertex_labels[v] = self.vertex_labels[v] | func(&self, v);
        }
    }
}
// Iterator implementation
impl<'a> Iterator for GraphIter<'a> {
    type Item = (Idx, &'a [Edge]);
    fn next(&mut self) -> Option<Self::Item> {
        if self.idx == self.g.vertex_count {
            None
        } else {
            let result = Some((self.idx, self.g.get_edges(self.idx)));
            self.idx = self.idx + 1;
            result
        }
    }
}

/// free function that tests if a given triangle is in fact a love triangle
pub fn is_love_triangle(g: &Graph, a: Idx, b: Idx, c: Idx) -> bool {
    let pos = (
        g.weight(a, b).unwrap_or(0),
        g.weight(b, c).unwrap_or(0),
        g.weight(c, a).unwrap_or(0),
    );

    let neg = (
        g.weight(a, c).unwrap_or(0),
        g.weight(b, a).unwrap_or(0),
        g.weight(c, b).unwrap_or(0),
    );

    (pos.0 > 0) && (pos.1 > 0) && (pos.2 > 0) && (neg.0 < 0) && (neg.1 < 0) && (neg.2 < 0)
}

#[cfg(test)]
mod test {
    use super::*;
    /// A brief sanity check that the triangle enumeration algorithm works
    #[test]
    fn sanity_check() {
        let graph = Graph {
            vertex_count: 3,
            adj_span: vec![(0, 1), (1, 1), (2, 1)],
            adj_list: vec![(1, 10), (2, 10), (0, 10)],
            vertex_labels: vec![0; 3],
        };
        let triangle = graph.find_triangles(|_, _, _, _| true);
        assert_eq!(triangle.len(), 1);
    }

    /// Test for distinct triangles, e.g. those that don't share an edge
    #[test]
    fn distinct_triangles() {
        let elements: &[(Idx, &[(Idx, Weight)])] = &[
            (0, &[(1, 0)]),
            (1, &[(2, 0)]),
            (2, &[(0, 0)]),
            (3, &[(4, 0)]),
            (4, &[(5, 0)]),
            (5, &[(3, 0)]),
        ];

        let triangles = elements
            .iter()
            .map(|i| *i)
            .collect::<Graph>()
            .find_triangles(|_, _, _, _| true);

        assert_eq!(triangles.len(), 2);
    }

    /// Test for triangles with shared edges
    #[test]
    fn overlapping_triangles() {
        let elements: &[(Idx, &[(Idx, Weight)])] = &[
            (0, &[(1, 0)]),
            (1, &[(2, 0), (3, 0)]),
            (2, &[(0, 0)]),
            (3, &[(0, 0)]),
        ];

        let triangles = elements
            .iter()
            .map(|i| *i)
            .collect::<Graph>()
            .find_triangles(|_, _, _, _| true);

        assert_eq!(triangles.len(), 2);
    }

    /// Test for distinct + shared triangles with several
    /// vertices that are not members of any triangle
    #[test]
    fn works() {
        let elements: &[(Idx, &[(Idx, Weight)])] = &[
            (0, &[(1, 1), (2, -1)]),
            (1, &[(2, 1), (0, -1), (3, 0)]),
            (2, &[(0, 1), (1, -1)]),
            (3, &[(0, 0)]),
            (4, &[(0, 0)]),
            (5, &[(6, 0)]),
            (6, &[(7, 0)]),
            (7, &[(5, 0)]),
        ];

        let triangles = elements
            .iter()
            .map(|i| *i)
            .collect::<Graph>()
            .find_triangles(is_love_triangle);

        assert_eq!(triangles.len(), 1);
    }
}
